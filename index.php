<?php

use Metadata\Controller\MetadataController;

require __DIR__ . '/vendor/autoload.php';

$metadataController = new MetadataController();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    echo $metadataController->add();
} else {
    echo $metadataController->show();
}