<?php

namespace Metadata\Controller;

use Metadata\Helper\DataRetriever;
use Metadata\Model\MetadataModel;
use Metadata\View\MetadataView;

class MetadataController
{
    const TITLE_LENGTH = 75;
    const DESCRIPTION_LENGTH = 160;
    protected MetadataView $view;
    protected MetadataModel $model;
    protected DataRetriever $retriever;

    public function __construct()
    {
        $this->view = new MetadataView();
        $this->model = new MetadataModel();
        $this->retriever = new DataRetriever();
    }

    public function show(): string
    {
        return $this->view->prepareView(visited: $this->model->getData());
    }

    public function add(): string
    {
        $params = $this->validateInput($_POST);
        if (isset($params['alert'])) {
            return $this->view->prepareView($params, $this->model->getData());
        }
        $this->model->addUrl($params['url_input']);
        $data = $this->retriever->retrieveDataFromUrl($params['url_input']);
        $title = $this->retriever->retrieveTitleFromUrl($params['url_input']);
        $params = array_merge($params, $this->validateData($data, $title));
        return $this->view->prepareView($params, $this->model->getData());
    }

    protected function validateInput($params = [])
    {
        $result = [];
        if (empty($params['url_input'])) {
            $result['alert'] = 'Nie podano adresu URL!';
            return $result;
        }
        $result['url_input'] = filter_var($params['url_input'], FILTER_VALIDATE_URL);
        if ($result['url_input'] === false) {
            $result['alert'] = 'Zły format URL!';
            $result['url_input'] = $params['url_input'];
        }
        return $result;
    }

    protected function validateData(array $data = [], string $title = '')
    {
        $result = [];
        if ($data === false) {
            $result['alert'] = 'Nie znaleziono strony!';
            return $result;
        }
        if ($title === false) {
            $result['alert'] = 'Nie znaleziono strony!';
            return $result;
        }
        $alert = '';
        $result['meta_title'] = $title;
        if (strlen($title) > self::TITLE_LENGTH) $alert .= 'Zbyt długi tytuł.';
        $result['meta_description'] = $data['description'];
        if (strlen($data['description']) > self::DESCRIPTION_LENGTH) $alert .= 'Zbyt długi opis.';
        if (!empty($alert)) $result['alert'] = $alert;
        return $result;
    }
}