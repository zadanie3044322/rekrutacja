<?php

namespace Metadata\Model;

class MetadataModel
{
    const FILENAME = __DIR__ . '/../../meta.txt';
    protected array $data;

    protected function loadDataFromFile(): array
    {
        if (!empty($this->data)) return $this->data;
        $lines = file(self::FILENAME, FILE_IGNORE_NEW_LINES);
        $result = [];
        if ($lines !== false) {
            foreach ($lines as $line) {
                $items = explode(',', $line);
                $result[] = ['url' => $items[0], 'date' => $items[1]];
            }
        }
        $this->data = $result;
        return $result;
    }

    protected function saveDataToFile()
    {
        $data = implode("\n", array_map(function ($item) {
            return implode(",", $item);
        }, $this->data));
        file_put_contents(self::FILENAME, $data);
    }

    public function addUrl(string $url)
    {
        $this->loadDataFromFile();
        $this->data[] = ['url' => addslashes($url), 'date' => date('Y-m-d H:i:s')];
        if (count($this->data) > 10) array_shift($this->data);
        $this->saveDataToFile();
    }

    public function getData()
    {
        return $this->loadDataFromFile();
    }
}