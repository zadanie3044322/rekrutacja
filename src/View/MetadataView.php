<?php

namespace Metadata\View;

use Metadata\Controller\MetadataController;

class MetadataView
{
    public function prepareView(array $params = [], array $visited = [])
    {
        return $this->prepareForm($params) . $this->prepareVisitedList($visited);
    }

    protected function prepareForm(array $params = []): string
    {
        return '<form action="" method="post">
                    <h2>URL</h2>
                    <input type="text" name="url_input" style="width: 500px" value="' . ($params['url_input'] ?? '')
            . '"><button type="submit">Pobierz</button>
                    <span style="color: red">' . ($params['alert'] ?? '') . '</span><br>
                </form>
                <h2>Tytuł strony - meta title</h2>
                <span>' . strlen($params['meta_title'] ?? '') . '/' . MetadataController::TITLE_LENGTH
            . ' znaków</span><br>
                <textarea name="meta_title" placeholder="Napisz tytuł opisujący treść i zachęcający do kliknięcia" '
            . ' style="width: 500px" disabled>' . ($params['meta_title'] ?? '') . '</textarea><br>
                <h2>Opis strony - meta description</h2>
                <span>' . strlen($params['meta_description'] ?? '') . '/' . MetadataController::DESCRIPTION_LENGTH
            . ' znaków</span><br>
                <textarea name="meta_description" placeholder="Użyj opisu meta aby opisać zawartość Twojej strony.'
            . 'Przygotowana treść może być wyświetlana przez Google w wynikach wyszukiwania jeśli zostanie uznana '
            . 'za dokładniejszą niż treść ze strony. Pamiętaj, że dobre opisy mogą poprawić klikalność - wpłynąć '
            . 'na liczbę odwiedzin." style="width: 500px" disabled>'
            . ($params['meta_description'] ?? '') . '</textarea><br>';
    }

    protected function prepareVisitedList(array $visited = [])
    {
        $view = '<h2>Odwiedzone</h2>
                <table>';
        foreach ($visited as $value) {
            $view .= '<tr><td>' . $value['url'] . '</td><td>' . $value['date'] . '</td></tr>';
        }
        $view .= '</table>';
        return $view;
    }
}