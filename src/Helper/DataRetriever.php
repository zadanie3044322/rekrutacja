<?php

namespace Metadata\Helper;
class DataRetriever
{
    public function retrieveDataFromUrl(string $url): array|bool
    {
        if (!filter_var($url)) return false;
        return get_meta_tags($url);
    }

    public function retrieveTitleFromUrl(string $url): string|bool
    {
        if (!filter_var($url)) return false;
        $page = file_get_contents($url);
        preg_match("/<title>(.*)<\/title>/", $page, $title);
        if (!empty($title)) return strip_tags($title[0]);
        return '';
    }
}